package lv.TicTacToe;

import java.util.Scanner;


public class HumanPlayer implements Player{
	
	Figures figure = Figures.CROSS;

	public void turn(Field field){
		

		Scanner input = new Scanner( System.in );
		System.out.println("Human Turn");
		System.out.println("input x");
		int x = input.nextInt();
		System.out.println("input y");
		int y = input.nextInt();
		
		while (field.getField()[x][y] != Figures.NULL) {
			System.out.println("This cell is already set");
			System.out.println("input another x");
			x = input.nextInt();
			System.out.println("input another y");
			y = input.nextInt();
		}
		
		field.setCell(x, y, figure);
		
	
	}

}
