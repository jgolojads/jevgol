package lv.TicTacToe;

public class Main {

	/**
	 * @param args
	 */
	static Game ticTacToe;
	Player[] players;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ticTacToe = new Game(2, 3, 3);
		ticTacToe.play();
	}

}

/* 
 NOTE! DIAGONAL HITS ARE NOT COUNTED AS WIN. My current objective is to start using objects and learning Java.

 * EXECUTION RESULTS BELOW *

_ _ _ 
_ _ _ 
_ _ _ 
Human Turn
input x
1
input y
1
_ _ _ 
_ X _ 
_ _ _ 
AI Turn
AI set x to: 1
AI set y to: 0
_ _ _ 
O X _ 
_ _ _ 
Human Turn
input x
1
input y
1
This cell is already set
input another x
1
input another y
0
This cell is already set
input another x
2
input another y
1
_ _ _ 
O X _ 
_ X _ 
AI Turn
AI set x to: 2
AI set y to: 0
_ _ _ 
O X _ 
O X _ 
Human Turn
input x
0
input y
1
_ X _ 
O X _ 
O X _ 
Game Over
 * */
