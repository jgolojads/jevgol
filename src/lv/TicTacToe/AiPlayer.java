package lv.TicTacToe;

import java.util.Random;

public class AiPlayer implements Player{
	
	Figures figure = Figures.ZERO;
	
public void turn(Field field){
		
		Random generator = new Random();
		System.out.println("AI Turn");
		int x = generator.nextInt(3);
		int y = generator.nextInt(3);
		
		while (field.getField()[x][y] != Figures.NULL) {
			x = generator.nextInt(3);
			y = generator.nextInt(3);	
		}	
		
		
		System.out.println("AI set x to: "+ x);
		System.out.println("AI set y to: "+ y);

		field.setCell(x, y, figure);
	
	}
	
}
