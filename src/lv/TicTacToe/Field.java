package lv.TicTacToe;

public class Field {

	/**
	 * @param args
	 */
	Figures figure = Figures.NULL;
	Figures[][] table; 
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public Field(int x, int y){
		table = new Figures[x][y];

		for(int i=0; i < x; i++) {
			for(int j=0; j < y; j++) {
				table[i][j] = figure;
				System.out.print("_ ");
			}
			System.out.println();
		}

		
	}
	
	public Figures[][] getField(){
		return table;
	}
	
	
	public void setCell (int x, int y, Figures the_figure){
		table[x][y] = the_figure;
	}
	
	public void showField(Figures[][] table){
		for(int i=0; i < table.length; i++) {
			for(int j=0; j < table[i].length; j++) {

				switch ( table[i][j] ) {
				case CROSS:
					System.out.print("X ");
					break;
				case ZERO:
					System.out.print("O ");
					break;
				case NULL:
					System.out.print("_ ");
					break;
				}
				
				
				
				
			}
			System.out.println();
		}

	}

}
