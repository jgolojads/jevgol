package lv.TicTacToe;

public class Game {

	/**
	 * @param args
	 */

	static Player aiPlayer;
	static Player humanPlayer;
	static Field field;
	static int cellsCount;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public Game(int playerCount, int fieldsX, int fieldsY) {

		aiPlayer = new AiPlayer();
		humanPlayer = new HumanPlayer();
		field = new Field(fieldsX, fieldsY);
		cellsCount = fieldsX * fieldsY;

	}

	public void play() {

		int step = 0;
		while (!gameOver(cellsCount)) {
			step++;
			if (step % 2 == 0) {
				aiPlayer.turn(field);
				field.showField(field.getField());
			} else {
				humanPlayer.turn(field);
				field.showField(field.getField());
			}

		}
		System.out.println("Game Over");

	}

	public static boolean gameOver(int cellsCount) {

		boolean result = false;

		result = validate(field.getField(), cellsCount);

		return result;
	}

	public static boolean validate(Figures[][] table, int cellsCount) {

		boolean result = false;
		int rowSum = 0;
		int colSum = 0;
		int increment = 0;

		for (int i = 0; i < table.length; i++) {
			
			for (int j = 0; j < table[i].length; j++) {
				
				switch ( table[i][j] ) { // single row Sum
				case CROSS:
					rowSum ++;
					increment++; 
					break;
				case ZERO:
					rowSum --;
					increment++;
					break;
				case NULL:
					rowSum = 0;
					break;
				}
				
				
				switch ( table[j][i] ) { // single column Sum
				case CROSS:
					colSum ++;
					break;
				case ZERO:
					colSum --;
					break;
				case NULL:
					colSum = 0;
					break;
				}
				
			}

			if ((rowSum == 3) || (rowSum == -3) || (increment == cellsCount) || (colSum == 3) || (colSum == -3)) {
				result = true;
			}
			
			rowSum = 0;
			colSum = 0;
			
		}



		return result;
	}

}
